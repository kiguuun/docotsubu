package model;

import dao.MutterDAO;

/**つぶやきの投稿に関する処理を行うモデル(DAOを利用)
 * @author 200217AM
 *
 */
public class PostMutterLogic {
	public void excute(Mutter mutter){
		MutterDAO dao = new MutterDAO();
		dao.create(mutter);
	}
}
