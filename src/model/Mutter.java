package model;
//つぶやきに関する情報を持つJavaBeans

import java.io.Serializable;

public class Mutter implements Serializable{
	private int id;
	private String userName;//ユーザー名
	private String text;//つぶやき内容
	private String today;

	public Mutter(){}
	public Mutter(int id, String userName,String text,String today){
		this.id = id;
		this.userName = userName;
		this.text = text;
		this.today = today;
	}


	public Mutter(String userName, String text, String today) {
		super();
		this.userName = userName;
		this.text = text;
		this.today = today;
	}

	public Mutter(int id, String userName, String text) {
		super();
		this.id = id;
		this.userName = userName;
		this.text = text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getToday() {
		return today;
	}
	public void setToday(String today) {
		this.today = today;
	}


}
