package model;

import java.util.List;

import dao.MutterDAO;

/**つぶやきの取得に関する処理を行うモデル
 * @author 200217AM
 *
 */
public class GetMutterListLogic {
	public List<Mutter> excute(){
		MutterDAO dao = new MutterDAO();
		List<Mutter> mutterList = dao.findAll();
		return mutterList;
	}

}
