package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Mutter;

public class MutterDAO {
	//データベース接続に使用する情報
	 // ------JDBCコピペ1ここから-----------------------//メソッドの外に貼る
    final String DRIVER_NAME = "com.mysql.jdbc.Driver";// MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/";// DBサーバー名
    final String DB_NAME = "docotsubu";// データベース名、使いたいデータベースによって変える
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";// 文字化け防止
    final String JDBC_URL = DB_URL + DB_NAME + DB_ENCODE;// 接続DBとURL
    final String DB_USER = "root";// ユーザーID
    final String DB_PASS = "root";// パスワード
    // -------JDBCコピペ1ここまで------------------------

    public List<Mutter> findAll(){
    	List<Mutter> mutterList = new ArrayList<Mutter>();
    	// JDBCコピペ2ここから--------------------------------//メソッドの中に貼る
        try
        {
                Class.forName(DRIVER_NAME);
        }catch(
        ClassNotFoundException e)
        {
                e.printStackTrace();//エラー内容を表示
        }
        // JDBCコピペ2ここまで--------------------------------
        try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)) {
			//select文の準備
		    String sql = "SELECT * FROM mutter ORDER BY ID DESC";
		    PreparedStatement pStmt = conn.prepareStatement(sql);
		    //select文を実行
		    ResultSet rs = pStmt.executeQuery();
		    //select文の実行結果をListに格納
		    while(rs.next()){
		    	int id = rs.getInt("id");
		    	String name = rs.getString("name");
		    	String text = rs.getString("text");
		    	Mutter mutter = new Mutter(id,name,text);
		    	mutterList.add(mutter);
		    }

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}


        return mutterList;

    }

    public boolean create(Mutter mutter){

    	// JDBCコピペ2ここから--------------------------------//メソッドの中に貼る
        try
        {
                Class.forName(DRIVER_NAME);
        }catch(
        ClassNotFoundException e)
        {
                e.printStackTrace();//エラー内容を表示
        }
        // JDBCコピペ2ここまで--------------------------------
        try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)) {
			//insert文の準備
		    String sql = "INSERT INTO mutter(name,text) values(?,?)";
		    PreparedStatement pStmt = conn.prepareStatement(sql);
		    //insert文に？に設定する値を決める
		    pStmt.setString(1, mutter.getUserName());
		    pStmt.setString(2, mutter.getText());
		    //insert文を実行
		    int result = pStmt.executeUpdate();
		    if(result !=1){
		    	return false;
		    }


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}


        return true;

    }
}
